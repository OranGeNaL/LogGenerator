using System.Runtime.CompilerServices;

using Confluent.Kafka;
using System.Net;

public static class KafkaProducer
{
    private static ProducerConfig config;
    private static IProducer<Null, string> producer;

    private static string? _servers = "";
    private static string? _topic = "";

    public static void Initialize()
    {
        _servers = Environment.GetEnvironmentVariable("KAFKA_SERVERS");
        _topic = Environment.GetEnvironmentVariable("TOPIC");
        try
        {
            config = new ProducerConfig
            {
                BootstrapServers = _servers,
                ClientId = Dns.GetHostName()
            };

            producer = new ProducerBuilder<Null, string>(config).Build();
        }
        catch (Exception e)
        {
            // Console.WriteLine(e);
        }
    }

    public static async void ProduceMessage(string message)
    {
        try
        {
            await producer.ProduceAsync(_topic, new Message<Null, string> { Value=message });
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
    
}