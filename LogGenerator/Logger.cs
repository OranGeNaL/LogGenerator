using System.Text;


public static class Logger
{
    public static void Log(string host, string message, string logLevel)
    {
        string fullMessage = $"{DateTime.Now:O} | {logLevel} | {host} | {message}";
        
        Console.WriteLine(fullMessage);
        KafkaProducer.ProduceMessage(fullMessage);
    }

    public static string Info = "[INFO] ";
    public static string Debug = "[DEBUG]";
    public static string Error = "[ERROR]";
}