﻿using System.Net;
using System.Threading;


KafkaProducer.Initialize();
Random random = new Random();
string host = Dns.GetHostName();
while (true)
{
    switch (random.NextInt64(0, 3))
    {
        case 0:
            var d1 = random.NextInt64(10, 100);
            var d2 = random.NextInt64(0, 4);
            Logger.Log(host, $"Получены числа {d1} и {d2}", Logger.Info);
            try
            {
                var result = d1 / d2;
                Logger.Log(host, $"Результат деления {d1} на {d2} равен {result}", Logger.Info);
            }
            catch (Exception e)
            {
                Logger.Log(host, $"Ошибка при делении {d1} на {d2}", Logger.Error);
                Logger.Log(host, e.Message, Logger.Error);
            }
            break;
        case 1:
            Logger.Log(host, $"Создание нового рандомайзера", Logger.Info);
            Random possibleRandomiser = null;

            var willBeCreated = random.NextInt64(0, 4);
            if (willBeCreated != 0)
            {
                possibleRandomiser = new Random();
            }

            try
            {
                Logger.Log(host, $"Новый рандомайзер сгенерировал число {possibleRandomiser.NextInt64(0, 1231123)}", Logger.Info);
            }
            catch (Exception e)
            {
                Logger.Log(host, $"Рандомайзер не был создан", Logger.Error);
                Logger.Log(host, e.Message, Logger.Error);
            }
            
            break;
        case 2:
            int[] massive = new[] {13, 324, 75, 4123, 65, 546, 547, 687, 274, 10};
            int ind = (int) (random.NextInt64(0, 15));

            try
            {
                Logger.Log(host, $"Найден {ind} элемент массива: {massive[ind]}", Logger.Info);
            }
            catch (Exception e)
            {
                Logger.Log(host, $"Выход за предел массива: {ind}", Logger.Error);
                Logger.Log(host, e.Message, Logger.Error);
            }
            
            break;
    }

    int pause = (int) (random.NextInt64(4, 10));
    Logger.Log(host, $"Ожидание {pause} секунд", Logger.Debug);
    Thread.Sleep(pause * 1000);
}